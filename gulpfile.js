const { src, dest, watch, parallel } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const uglify = require("gulp-uglify");
const imagemin = (await import("imagemin")).default;
const imagemin = require("gulp-imagemin");
const rename = require("gulp-rename");
const connect = require("gulp-connect");

function html() {
    return src("src/templates/**/*.html").pipe(dest("dist"));
}

function styles() {
    return src("src/styles/main.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(dest("dist"))
        .pipe(connect.reload());
}

function scripts() {
    return browserify("src/scripts/app.js")
        .transform(
            babelify.configure({
                presets: ["@babel/preset-env"],
            })
        )
        .bundle()
        .pipe(source("bundle.js"))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(dest("dist"))
        .pipe(connect.reload());
}

function img() {
    let dest = src("./assets");

    if (!(process.env.NODE_ENV === "local")) {
        dest = dest + "/img";
    }

    return gulp
        .src(paths.img.src)
        .pipe(gulpif(isProduction, imagemin()))
        .pipe(gulp.dest("./assets"))
        .pipe(connect.reload());
}

function server() {
    connect.server({
        root: "dist",
        livereload: true,
        port: 3000,
    });
}

function sentinel() {
    watch("src/templates/**/*.html", { ignoreInitial: false }, html);
    watch("src/styles/**/*.scss", { ignoreInitial: false }, styles);
    watch("src/scripts/**/*.js", { ignoreInitial: false }, scripts);
    watch("public/images/**/*.{png,jpg,gif}", { ignoreInitial: false }, img);
}

exports.default = parallel(server, sentinel);
