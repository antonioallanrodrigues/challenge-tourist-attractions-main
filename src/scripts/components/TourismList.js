export class TourismList {
    constructor() {
        this.list = [];
        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".add-tourist-spot-container");
        this.title = document.querySelector(".add-tourist-spot-title");
        this.description = document.querySelector(".add-tourist-spot-description");
        this.items = document.querySelector(".tourism-spots-banners");
        this.photo = document.getElementById("imagemNova");
        this.file = document.getElementById("image-file-input");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));
    }

    addItemToList(event) {
        event.preventDefault();

        let formData = new FormData();
        formData.append("avatar", event.target[0].files[0]);
        fetch("http://localhost:7755/api/avatar", { method: "post", body: formData })
            .then((res) => res.json())
            .then((res) => {
                avatar.src = `http://localhost:7755${res.payload.url}`;
                console.log(res.payload);
            })
            .catch(console.log);

        let bannerTitle = event.target["title-title"].value;
        console.log(bannerTitle);
        let bannerDescription = event.target["description-description"].value;
        console.log(bannerDescription);

        if (bannerTitle !== "" && bannerDescription !== "") {
            const item = {
                title: bannerTitle,
                description: bannerDescription,
            };

            this.list.push(item);

            this.renderListItems();

            // this.previewFile();

            this.resetInputs();
        }
    }

    renderListItems() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
                <li class="tourism-spots-banners-image-description">
                    <div class="tourism-spots-banners-image-container">
                        <img id="imagemNova" class="tourism-spots-banners-image" src="http://localhost:7755${res.payload.url}">
                    </div>
                    <div class="tourism-spots-banners-title-description">
                        <h4 class="tourism-spots-banners-title">${item.title}</h4>
                        <p class="tourism-spots-banners-description">${item.description}</p>
                    </div>
                  </li>
            `;
        });

        this.items.innerHTML = itemsStructure;
    }

    // previewFile() {
    //     this.preview = document.querySelector(".tourism-spots-banners-image");
    //     this.file = document.querySelector(".add-tourist-spot-image").files[0];
    //     this.reader = new FileReader();
    //     this.reader.addEventListener(
    //         "load",
    //         function () {
    //             // convert image file to base64 string
    //             this.preview.src = this.reader.result;
    //         },
    //         false
    //     );

    //     if (this.file) {
    //         this.reader.readAsDataURL(this.file);
    //     }
    // }

    resetInputs() {
        this.title.value = "";
        this.description.value = "";
    }
}
